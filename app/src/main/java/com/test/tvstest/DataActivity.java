package com.test.tvstest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataActivity extends AppCompatActivity {

    TextView data_name_value, data_profile_loc, data_date, data_salary, image_time;
    DataPOJO dataPOJO;
    Button image_upload;
    AppCompatImageView imageView;


    private static final int CAMERA_REQUEST = 1888;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Layout setup
        setContentView(R.layout.data_activity);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }

//        Layout views setup
        data_name_value = findViewById(R.id.data_name_value);
        data_profile_loc = findViewById(R.id.data_profile_loc);
        data_date = findViewById(R.id.data_date);
        data_salary = findViewById(R.id.data_salary);
        image_time = findViewById(R.id.image_time);
        imageView = findViewById(R.id.imageView);
        image_upload = findViewById(R.id.image_upload);

//        pojo data from intent
        dataPOJO = (DataPOJO) getIntent().getSerializableExtra("data");

//        Updating/setting the data to be shown on the views
        data_name_value.setText(dataPOJO.getName().concat(" ").concat("(").concat(dataPOJO.getValue()).concat(")"));
        data_profile_loc.setText(dataPOJO.getProfile().concat(" ").concat("(").concat(dataPOJO.getLocation()).concat(")"));
        data_date.setText(dataPOJO.getDate());
        data_salary.setText(dataPOJO.getSalary());

//        upload button listener
        image_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capture();
            }
        });
    }

    public void capture() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        checks if the image is return from camera ctivity or not
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
//            fetch data and store as bitmap
            Bitmap photo = (Bitmap) data.getExtras().get("data");

//            Changing the captured image quality and breaking it in strams
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.PNG, 100, stream);

//            setup bitmap in the imageview
            imageView.setImageBitmap(photo);

//            showing the time when image was captured
            image_time.setVisibility(View.VISIBLE);
            image_time.setText(getTimeDate(System.currentTimeMillis()));


        }
    }


//    this function formats the millisecond in correct date time format
    public String getTimeDate(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");

        Date resultdate = new Date(time);
        return sdf.format(resultdate);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
