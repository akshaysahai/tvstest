package com.test.tvstest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText password_et, username_et;
    Button login_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Layout initialization
        setContentView(R.layout.activity_main);
//        Layout views initialization
        username_et = findViewById(R.id.username_et);
        password_et = findViewById(R.id.password_et);
        login_btn = findViewById(R.id.login_btn);

//        Login button click listener
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Checks if the username & password is filled
                if (!isTextEmpty(username_et) && !isTextEmpty(password_et)) {
//                    Continue only if both the fields are filled
                    startActivity(new Intent(LoginActivity.this, DataListActivity.class));
                } else {
                    Toast.makeText(LoginActivity.this, "Fields cannot be left empty.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

//    function to check the length of input and returns true if its empty and false otherwise
    public boolean isTextEmpty(EditText editText) {
        return editText.getText().toString().trim().length() == 0;
    }

}
