package com.test.tvstest;

import java.io.Serializable;

public class DataPOJO implements Serializable {

//    Get Set/ POJO class for the response being sent in the JSON response.
    String name;
    String profile;
    String location;
    String value;
    String date;
    String salary;

    public DataPOJO(String name, String profile, String location, String value, String date, String salary) {
        this.name = name;
        this.profile = profile;
        this.location = location;
        this.value = value;
        this.date = date;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }
}
