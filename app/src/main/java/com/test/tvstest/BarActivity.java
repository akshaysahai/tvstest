package com.test.tvstest;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class BarActivity extends AppCompatActivity {

    BarChart barChart;
    HashMap<Long, DataPOJO> dataInfoTreeMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bar_activtiy);

//        back arrow setup
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }


        barChart = findViewById(R.id.chart);
        dataInfoTreeMap = (HashMap<Long, DataPOJO>) getIntent().getExtras().get("data");

        List<BarEntry> entries = new ArrayList<>();

        for (Map.Entry<Long, DataPOJO> entry : dataInfoTreeMap.entrySet()) {
            // turn your data into Entry objects
            entries.add(new BarEntry(Float.valueOf(entry.getKey()),
                    Float.valueOf(entry.getValue().getSalary().substring(1).replaceAll(",",""))));
        }
        BarDataSet barData = new BarDataSet(entries,"");
        BarData data = new BarData(barData);
        data.setBarWidth(0.9f); // set custom bar width

        barChart.setData(data);
        barChart.setFitBars(false); // make the x-axis fit exactly all bars
        barChart.invalidate(); // refresh

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
