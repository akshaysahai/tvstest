package com.test.tvstest;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.TreeMap;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DataListActivity extends AppCompatActivity {

    RecyclerView search_recyclerview;
    TreeMap<Long, DataPOJO> dataInfoTreeMap;
    TreeMap<Long, DataPOJO> filteredTreeMap;
    SearchRecyclerViewAdapter searchRecyclerViewAdapter;
    AppCompatEditText search_editText;
    Toolbar search_toolbar;
    TextView blank_search_hint_tv;
    Button bar_btn;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        layout setup
        setContentView(R.layout.search_activity);

//        toolbar setup
        search_toolbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(search_toolbar);

//        back arrow setup
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp);
            upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }

        progressDialog = new ProgressDialog(this);

//recyclerview initialization
        search_recyclerview = findViewById(R.id.search_recyclerview);
//        treemaps initialization
        dataInfoTreeMap = new TreeMap<>();
        filteredTreeMap = new TreeMap<>();
//        recyclerview layout manager setup
        search_recyclerview.setLayoutManager(new LinearLayoutManager(this));
        searchRecyclerViewAdapter = new SearchRecyclerViewAdapter(dataInfoTreeMap);
//        recyclerview adapter setup
        search_recyclerview.setAdapter(searchRecyclerViewAdapter);

//            data fetch function is called
        if (ConnectedOrNot(DataListActivity.this)) {
            postRequester();
        } else {
            Toast.makeText(DataListActivity.this, "Not connected to the internet", Toast.LENGTH_SHORT).show();
        }

        search_editText = findViewById(R.id.search_editText);
        bar_btn = findViewById(R.id.bar_btn);
        blank_search_hint_tv = findViewById(R.id.blank_search_hint_tv);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            search_editText.setFocusedByDefault(true);
        }

//        edittext change listener
        search_editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            //            this function is called everytime user enters anything, this is responsibe for filtering the list of information
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() > 0) {
                    filteredTreeMap = new TreeMap<>();
                    long i = 0;
                    for (Map.Entry<Long, DataPOJO> entry : dataInfoTreeMap.entrySet()) {
                        DataPOJO value = entry.getValue();
                        long key = entry.getKey();
                        if (value.getName().toLowerCase().contains(editable.toString().toLowerCase()) ||
                                value.getName().toLowerCase().contains(editable.toString().toLowerCase()) ||
                                value.getLocation().toLowerCase().contains(editable.toString().toLowerCase())) {
                            filteredTreeMap.put(i, value);
                            i++;
                        }
                    }
                    searchRecyclerViewAdapter = new SearchRecyclerViewAdapter(filteredTreeMap);
                    searchRecyclerViewAdapter.notifyDataSetChanged();
                    search_recyclerview.setAdapter(searchRecyclerViewAdapter);
                } else {
                    searchRecyclerViewAdapter = new SearchRecyclerViewAdapter(dataInfoTreeMap);
                    searchRecyclerViewAdapter.notifyDataSetChanged();
                    search_recyclerview.setAdapter(searchRecyclerViewAdapter);
                }

            }
        });

//        sets the search drawable in the search bar for different android versions/makes it compatible
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Drawable leftDrawable = AppCompatResources
                    .getDrawable(this, R.drawable.ic_search_white_24dp);
            search_editText.setCompoundDrawablesWithIntrinsicBounds(leftDrawable, null, null, null);
        } else {
            Drawable leftDrawable = VectorDrawableCompat
                    .create(getResources(), R.drawable.ic_search_white_24dp, null);
            search_editText.setCompoundDrawablesWithIntrinsicBounds(leftDrawable, null, null, null);
        }

        bar_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectedOrNot(DataListActivity.this)) {
                    Intent intent = new Intent(DataListActivity.this, BarActivity.class);
                    intent.putExtra("data", dataInfoTreeMap);
                    startActivity(intent);
                } else {
                    Toast.makeText(DataListActivity.this, "Not connected to the internet", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    //    Internet check function, return true if connected
    public static boolean ConnectedOrNot(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }


    @SuppressLint("StaticFieldLeak")
    private void postRequester() {

        progressDialog.show();

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {

                String finalResponse = null;
                try {

                    OkHttpClient client = new OkHttpClient();

                    MediaType mediaType = MediaType.parse("application/octet-stream");
                    RequestBody body = RequestBody.create(mediaType, "{\n\"username\":\"test\",\n\"password\":\"123456\"\n}");
                    Request request = new Request.Builder()
                            .url("http://tvsfit.mytvs.in/reporting/vrm/api/test_new/int/gettabledata.php")
                            .post(body)
                            .addHeader("cache-control", "no-cache")
                            .build();

                    Response response = client.newCall(request).execute();
                    finalResponse = response.body().string();

                } catch (Exception ignored) {
                }
                return finalResponse;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            protected void onPostExecute(String responseString) {
                super.onPostExecute(responseString);
                try {
                    updateDataList(responseString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute();


    }

    //    this function is used to extract all information from json and then setting up recyclerview
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void updateDataList(String responseString) throws JSONException {

        if (progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        JSONObject jsonObject = new JSONObject(responseString);
        JSONObject jsonObject1 = new JSONObject((String) jsonObject.get("TABLE_DATA"));
        JSONArray jsonArray = jsonObject1.getJSONArray("data");

        for (int i = 0; i < jsonArray.length(); i++) {
            String name = jsonArray.getJSONArray(i).getString(0);
            String profile = jsonArray.getJSONArray(i).getString(1);
            String location = jsonArray.getJSONArray(i).getString(2);
            String value = jsonArray.getJSONArray(i).getString(3);
            String date = jsonArray.getJSONArray(i).getString(4);
            String salary = jsonArray.getJSONArray(i).getString(5);

            DataPOJO dataPOJO = new DataPOJO(name, profile, location, value, date, salary);
            dataInfoTreeMap.put((long) i, dataPOJO);
        }
        searchRecyclerViewAdapter = new SearchRecyclerViewAdapter(dataInfoTreeMap);
        search_recyclerview.setAdapter(searchRecyclerViewAdapter);

    }


    private class SearchRecyclerViewAdapter extends RecyclerView.Adapter<MyViewHolder> {

        TreeMap<Long, DataPOJO> recyclerviewTreeMap;

        public SearchRecyclerViewAdapter(TreeMap<Long, DataPOJO> recyclerviewTreeMap) {
            this.recyclerviewTreeMap = recyclerviewTreeMap;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(DataListActivity.this).inflate(R.layout.search_cards_layout, viewGroup, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {

            blank_search_hint_tv.setVisibility(View.INVISIBLE);

            myViewHolder.search_suggestion_title_tv.setText(recyclerviewTreeMap.get((long) i).getName());
            myViewHolder.search_suggestion_location_tv.setText(recyclerviewTreeMap.get((long) i).getLocation());

            myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DataListActivity.this, DataActivity.class);
                    intent.putExtra("data", recyclerviewTreeMap.get((long) i));
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return recyclerviewTreeMap.size();
        }
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {

        TextView search_suggestion_title_tv, search_suggestion_location_tv;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            search_suggestion_title_tv = itemView.findViewById(R.id.search_suggestion_title_tv);
            search_suggestion_location_tv = itemView.findViewById(R.id.search_suggestion_location_tv);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
